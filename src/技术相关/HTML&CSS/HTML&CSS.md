# 前端面试 - HTML/CSS

[TOC]

## 一、标签部分

## 1. HTML 与 HTML5 的区别是什么 ?

- HTML 需要在文档顶部使用 DTD 类型约束, HTML5 则不需要使用 DTD 类型约束而是直接在文档顶部使用 `<!DOCTYPE html>` 即可。【至于为什么 H5 可以不使用 DTD, 核心原因是 H5 不基于 SGML】

  ![image.png](https://upload-images.jianshu.io/upload_images/16761151-616eadda7738a1ca.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- HTML5 相比较 HTML 来说增加了许多特性, 并且其标签语义化的程度越来越高。

- HTML5 新增了许多 WEB API , 使网页的功能更加强大, 在不断加深标签语义化的概念的时候，会废弃冗余有歧义的标签。

### 2. DOCTYPE 是什么 ? 为什么要加它 ? 不加可以吗 ?

​	**DOCTYPE 是什么  :**

- DOCTYPE 是文档声明, 它是用来告诉浏览器将当前的文档以什么模式进行解析。【浏览器有两种模式解析文档(`标准模式`、`怪异模式`)】

​	**为什么要加它 :**

- 加上它就等于告诉浏览器以标准模式来解析当前的 HTML 文档。

​	**不加可以吗 :**

- 不加是可以的, 有些时候也看不出来效果上的差异, 但是此时浏览器是按照怪异模式来解析文档的而非是标准模式, 这就会导致网页可能会出现一些兼容性的问题。

**检测当前文档是标准模式还是怪异模式可以通过: `document.compatMode 来进行检测, 返回 CSS1Compat则代表标准模式, 返回 BackCompat 则代表是混杂模式【怪异模式】`**

### 3. CSS 怎么清除浮动 ?

- clear left/right/both。

  ```html
  <style>
      .box {
          float: left;
      }
  </style>
  <div id="container">
      <div class="box">1</div>
      <div class="box">2</div>
      <div class="box">3</div>
      <div class="forth-box">4</div>
  </div>
  ```

- 在受影响元素前面加上一个取消浮动的空 div。

  ```html
  <style>
      .box {
          float: left;
      }
  </style>
  <div id="container">
      <div class="box">1</div>
      <div class="box">2</div>
      <div class="box">3</div>
      <div style="clear: both"></div>
      <div class="forth-box">4</div>
  </div>
  ```

- 在浮动的元素的父盒子上固定高度。

  ```html
  <style>
      #container {
          height: 100px;
      }
  
      .box {
          float: left;
      }
  </style>
  <div id="container">
      <div class="box">1</div>
      <div class="box">2</div>
      <div class="box">3</div>
  </div>
  <div class="forth-box">4</div>
  ```

- 在浮动的元素的父盒子上使用 overflow: hidden;

  ```html
  <style>
      #container {
      	overflow: hidden;
      }
  
      .box {
          float: left;
      }
  </style>
  <div id="container">
      <div class="box">1</div>
      <div class="box">2</div>
      <div class="box">3</div>
  </div>
  <div class="forth-box">4</div>
  ```

  

### 4. 如何让一个元素在用户眼前消失 ?

- display: none;【元素消失,、不占空间、不能响应用户事件】
- opacity: 0; 【元素消失、占空间、可以响应用户事件】

- visibility: hidden; 【元素消失、占空间、不可以响应用户事件】
- position: absolute; 四个方向随便一个方向给一个足够大的值【元素消失、不占空间、不可以响应用户事件】 

### 5. 行内元素哪些 ? 块级元素呢 ?

- 常用的行内元素【a、b、br、i、span、small、big、sub、sup】
- 常用的块状元素【div、dl/dt/dd、h1 ~ h6、hr、ol/ul/li、p、table、form】
- 行内块元素【img、input】

**二者之间的区别 :**

- 块状元素 : 独占一行、 可以设置宽高、盒子模型属性皆可用。
- 行内元素 : 一行排列、不可以设置宽高、除了 margin-top、margin-right、margin-bottom 外其它盒子模型属性皆可用。

- 行内块元素 : 不独占一行、可以设置宽高、盒子模型属性皆可用。

**转换 :**

- display: block; -> 行内元素/行内块元素 => 块状元素

- display: inline; -> 块状元素/行内块元素 => 行内元素
- display: inline-block; -> 块状元素/行内元素 => 行内块元素【但是设置了 display: inline-block; 的元素会有一块缝隙, 这个缝隙可以通过给父元素设置 `font-size` 为 `0` , 再给每位子元素设置具体的 `font-size` 值即可】

### 6. 简单说下 link 与 @import 的区别 ?

- @import 属于 css 规范可能会存在浏览器的兼容问题【IE5+ 可以正常使用】，link 属于原生的 H5 标签, 不存在兼容问题。

- @import 和 link 加载的 css 资源都是当浏览器执行到的时候, 去请求对应资源, 只不过 link 比 @import 稍快【网上说的是 link 与页面同时加载, @import 是等到页面加载完毕再加载, 但笔者借助谷歌的 时间线 分析了好多次都不是这个结果, 所以这里笔者持保留意见还是相信自己的观点】。

  ![image.png](https://upload-images.jianshu.io/upload_images/16761151-042ebde20509af5a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- link 可以通过 js 来控制样式, 但是 @import 不能。
- link 除了可以引入 css 还可以实现 RSS 订阅、rel 连接属性等, 但@import 只能导入 css 资源。

### 7. CSS 如何实现垂直水平居中 ?

#### 定宽定高实现居中

- position + top/left 50% + -margin-top/left

- margin: auto; + absolute 【四个方向值为 0】

  ```html
  <style>
      .container {
          position: relative;
          width: 500px;
          height: 500px;
          background-color: aquamarine;
      }
  
      .container p {
          width: 150px;
          height: 20px;
          margin: auto;
          position: absolute;
          top: 0;
          bottom: 0;
          left: 0;
          right: 0;
          text-align: center;
          line-height: 20px;
      }
  </style>
  <div class="container">
      <p>测试居中 ...</p>
  </div>
  ```

  

#### 非定宽定高实现居中

- flex

  ```html
  <style>
      .container {
          display: flex;
          justify-content: center; /* 水平居中 */
          align-items: center; /* 垂直居中 */
          width: 500px;
          height: 500px;
          background-color: aquamarine
      }
  </style>
  <div class="container">
      <p>测试居中 ...</p>
  </div>
  ```

- flex + margin

  ```html
  <style>
      .container {
          display: flex;
          width: 500px;
          height: 500px;
          background-color: aquamarine;
      }
      .container p {
          margin: auto;
      }
  </style>
  <div class="container">
      <p>测试居中 ...</p>
  </div>
  ```

- translate + absolute

  ```html
  <style>
      .container {
          position: relative;
          width: 500px;
          height: 500px;
          background-color: aquamarine;
      }
  
      .container p {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
      }
  </style>
  <div class="container">
      <p>测试居中 ...</p>
  </div>
  ```

- display: table-cell; + text-align + vertical-align

  ```html
  <style>
      .container {
          display: table-cell;
          width: 500px;
          height: 500px;
          background-color: aquamarine;
          text-align: center;
          vertical-align: middle;
      }
  
      .container p {
      }
  </style>
  <div class="container">
      <p>测试居中 ...</p>
  </div>
  ```



### 8. 请简述 href 与 src 的区别

-1). **href : ** `href` 是指向网络资源所在位置, 通常使用在 【link、a】 等元素上, 是在当前元素与引用资源之间建立联系,所加载的资源不会对 HTML 的解析造成阻塞。

-2). **src :** `src` 是指向外部资源的位置,  通常使用在【img 、script、iframe】等元素上, 是将其所指向的资源下载过来然后来使用的, 在下载的过程中会阻塞 HTML 结构的解析。

### 9. 你知道 CSS 的选择器有哪些 ? 你知道他们的权重吗 ? 还有哪些 CSS3 新增的 ?

**css 选择器/权重 :**

!important: 权重『10000』

行内 style 属性: 【style=""】-> 权重『1000』

id 选择器: 【#box】 -> 权重『100』

class 选择器: 【.box】-> 权重『10』

property 选择器: 【[name=box]】『10』

伪类选择器: 【:nth-child(n)/:nth-last-child(n)/:link/:active/:hover/:focus/:visited/:before/:after】『10』

tag 选择器: 【div】『1』

后代选择器:【.box .child-box】『按上述所说权重依次相加』

子代选择器: 【.box>.child-box】『按上述所说权重依次相加』

兄弟选择器:【.box+.borther-box】『按上述所说权重依次相加』

相邻兄弟选择器: 【.box~.borther-box】『按上述所说权重依次相加』

权重计算规则: 先大后小、同级后覆、!import 最大

**CSS3 新增选择器 :**

- :root 选择文档根元素【通常是 html】
- :empty 选择没有任何子元素(包括文本节点)的某个元素

- :target 选择当前活动的锚点元素
- :enabled 选择每一个已启用的元素
- :disabled 选择每一个已禁用的元素
- :checked 选择每一个已选中的元素
- :not 选择非目标元素的元素



### 10. 说一说常见的浏览器及内核 

| 内核             | 简介                           | 代表                                                         | 兼容写法 |
| ---------------- | ------------------------------ | ------------------------------------------------------------ | -------- |
| Webkit           | Safari内核以及 Chrome 内核原型 | Chrome(28 版本以前)、Safari、Android 默认自带浏览器          | -webkit- |
| Trident          | IE 内核                        | IE6~7                                                        | -ms-     |
| Gecko            | FireFox 内核                   | FireFox                                                      | -moz-    |
| Presto           | Opera 内核                     | Opera 15 以前                                                | -o-      |
| Blink            | Chrome 最新内核                | Chrome 28 以后、Opera 15 以后                                | -webkit- |
| Webkit + Trident | 双内核                         | 各大国内浏览器【搜狗高速浏览器、360极速浏览器、2345浏览器、腾讯 TT 浏览器】 | -webkit- |

**浏览器市场份额 :**

![image.png](https://upload-images.jianshu.io/upload_images/16761151-6f21ba03194dc4f4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 11. 什么是标签语义化, 好处是什么 ?

- 标签语义化就是使用合理的标签做合理的事情。
- 好处 :
  - 文档结构清晰、便于维护。
  - 有利于 SEO 优化、便于搜索引擎抓取和网络推广。

### 12. 请你介绍一下 CSS 盒子模型并且简单介绍 box-sizing 

盒子模型的计算模式是 box-sizing 这个属性决定的。 

- box-sizing 默认属性值为 `content-box` , 也就是说在计算盒子的宽高的时候会将 padding 和 border 计算进去。

- box-sizing 另一个属性值为 `border-box` , 该属性与 `content-box` 行为相反, 是不会将 padding 和 border 计算进去。

**图解 :**

![image.png](https://upload-images.jianshu.io/upload_images/16761151-ebdf8de4a7c82646.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 盒子模型

  ![image.png](https://upload-images.jianshu.io/upload_images/16761151-7d20ee06498b0a45.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - padding 是内边距
  - margin 是外边距
  - border 是边框

三个属性都有 【top、bottom、left、right】属性值。

### 13. H5 的离线缓存怎么使用 ? 简单阐述下原理 ?

后面更新, 敬请期待 
loading ...

### 14. iframe 有哪些缺点 ?

- iframe 会阻塞主页面的 onload 事件。
- iframe 和主页面共享连接池，而浏览器对同源的连接有限制, 所以会影响页面的并行加载。
- 搜索引擎无法解析这种页面, 不利于 SEO 优化。
- 在某些移动设备, iframe 的框架无法显示完全, 存在一定的兼容性问题。
- 会增加很多页面, 导致不必要的开销。

### 15. 多个标签页之间是如何进行通信的 ?

后面更新, 敬请期待 
loading ...

### 16. 请在一个图片上实现一个圆形的可点击区域 ?

后面更新, 敬请期待 
loading ...

### 17. 你知道 CSS 中的继承吗 ? 哪些属性可以继承 ? 

CSS 中的某些属性是可以被子元素继承下来的, 这样有一个好处就是, 如果父元素有了某个样式, 恰好这个样式在他的后辈元素中也会用到, 基于这种继承机制我们什么都不用做, 默认某些可继承的属性就会被子元素所继承。

CSS 中支持继承特性的属性有 :

- 字体系列属性
  - font-size
  - font-family
  - font-weight
  - font-style
- 文本系列属性
  - text-indent
  - text-align
  - line-height
  - word-spacing
  - letter-spacing
  - color
- 元素可见性
  - visibility
- 列表布局属性
  - list-style
  - list-style-type
  - list-style-image
- 光标属性
  - cursor

### 18. 说说你知道的几个 CSS HACK

**什么是 HACK ?**

> HACK 就是只有在特定浏览器才能识别这段 HACK 代码。HACK 是让前端头疼的问题，通俗说就是使用 CSS HACK 手段来处理 CSS 在各个浏览器中存在的兼容问题。

**CSS HACK 表现形式 :**

- CSS 属性前缀法 : 【IE 6能识别 "\_"、"\*", IE 7 能识别 "\*", 但不能识别 "\_", IE 6 ~ IE 10 都认识 "\9", 但是前面这些 FireFox 都不认】
- CSS 选择器前缀法 : 【IE 6能识别 "\*html" 、".class", IE 7 能识别 "\*+html" 或 "\*:first-child"】
- IE 条件注释法 : 【<!--[if IE]><![endif]-->】, 对写在判断语句里的所有代码在 IE 10下都生效。

-> IE 6 支持 【 _ 】`_color: green;`

-> IE 6( - )支持 : 【.test { -color: green; }】 

-> IE7 支持【 * 】`*color: green;`

-> 在选择器上设置只会在 IE6 中生效【*html .test { color: green; }】

-> IE(6/7/8) 都支持 : 【.test { color: green\9; }】

-> IE(8/9/10/11) 都支持 : 【.test { color: green\0; }】

-> IE 6/7( + )都支持 : 【.test { +color: green; }】

-> IE 6/7( \*+ ) : 【.test { *+color: green; }】

-> 在选择器上设置只会在 IE7 中生效【*+html .test { color: green; }】

-> 除IE 6 都支持 : 【.test { color: green !important; }】

-> IE 9/10 支持 : 【.test { color: green\9\0; }】

具体参看【[CSS HACKS](https://www.jb51.net/css/493362.html)】

### 19. transition 与 animation 的区别是什么 ?

- 区别一 : transition 需要触发条件不能自动执行，但是 animation 不需要触发条件可以借助 `@keyframes`

  自动执行。

- 区别二 : transition 默认只能触发一次, 如想触发多次则需要多次达到触发条件, animation 可以通过 `animation-iteration-count: infinite;` 设置无数次或者指定次数的自动触发。

- 区别三 : transition 只能设置开始和结束两个状态。animation 可以设置其他的中间状态。

- 区别四 : transition 为两帧两帧的, animation 可以为逐帧动画。

### 20. :before 与 ::before 的区别 ?

- 单冒号代表 CSS 中的伪类, 双冒号代表 CSS 中的伪元素。【CSS3 为了区分两者, 已经明确规定伪类用一个冒号, 伪元素用两个冒号，只不过单冒号的写法在 CSS 2 就已经有了, 所以涉及到兼容性处理的地方使用单冒号比较稳妥】**-** 也即不需要兼容 IE , 则遵循 CSS 3 规定。面对伪元素使用双冒号, 面对伪类依然使用单冒号。如果需要兼容 IE 则最好都是用单冒号。

- 伪类与伪元素对照表 :

**伪元素**

| 伪元素        | 作用                         |
| ------------- | ---------------------------- |
| :first-letter | 将特殊样式添加到文本的首字母 |
| :first-line   | 将特殊样式添加到文本的首行   |
| :before       | 在某元素之前插入某些内容     |
| :after        | 在某元素之后插入某些内容     |

**伪类:**

| 伪类                                 | 作用                                     |
| ------------------------------------ | ---------------------------------------- |
| :active                              | 将样式添加到被激活的元素                 |
| :focus                               | 将样式添加到聚焦的元素                   |
| :hover                               | 当鼠标悬浮在元素上方时, 为该元素添加样式 |
| :link                                | 将特殊的样式添加到未被访问过的链接       |
| :visited                             | 将特殊的样式添加到被访问过的链接         |
| :first-child                         | 将特殊样式添加到元素的第一个子元素       |
| :nth-child/:nth-last/first-child ... | ...                                      |

### 21. CSS 2.0 与 css 3.0 的区别 ?

- CSS 3 相比 CSS 2 来说更加强大。CSS 3 提供了许多新特性，比如: 选择器、盒模型、背景和边框、文字特效、2D/3D 转换、动画、多列布局、flex、媒体查询 等 ...
- 因为 CSS 3.0 目前还在处于开发阶段, 只不过一些浏览器厂商已经把部分特性提前的纳入到了浏览器的标准中, 所以相比较来说 CSS 2.0 更加稳定, 兼容性也比 CSS 3.0 要好。 



### 22. 了解 flex 布局吗 ? 他都有那些属性分别可以用来做什么 ?

![image.png](https://upload-images.jianshu.io/upload_images/16761151-1b3afb252340c3f9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- flex 是弹性布局, 为和模型提供了强大的灵活性。

- 任何一个容器都可以指定为: flex 布局, 包括行内元素【.ele { display: inline-flex; }】
- 容器默认有两根轴: 水平的主轴和垂直交叉轴【水平主轴的起始位置叫做 main start, 终止位置叫做 main end; 垂直交叉轴的起始位置叫做 cross start, 结束位置叫做 cross end】。

- 项目默认按照主轴排列, 单个项目占据主轴空间叫做: main size。占据交叉轴空间叫做: cross size

- flex 有 6 个属性 : 【flex-direction、flex-wrap、flex-flow、justify-content、align-items、align-content】
  - flex-direction 决定主轴的方向
    - row 【从左到右】
    - row-reverse【从右到左】
    - column 【从上到下】
    - column-reverse【从下到上】

![image.png](https://upload-images.jianshu.io/upload_images/16761151-83608f79c3aa2789.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/16761151-96c32439e0b0325f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/16761151-470ccb118fa205ee.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/16761151-b2839fb67c935af4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 
  - flex-wrap 规定如果一行排不下该如何换行

    - nowrap 【不换行】
    - wrap 【换行 - 第一行在上方】
    - wrap-reverse【换行 - 第一行在下方】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-eaad00e3b6c6d4a5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-59178bd25bd4e0c9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-e784dc881f956de1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - flex-flow 是 flex-direction 与 flex-wrap 的简写形式
    - 默认值是 : `flex-flow: row nowrap;` 。  
    - 其他形式 : 自行脑补 

  - justify-content 项目在主轴上的对齐方式

    - flex-start 【从左到右 - 紧密排列】
    - flex-end【从右到左 - 紧密排列】
    - center【水平居中 - 紧密排列】
    - space-between【项目均匀分散排列 - 两端无留白】
    - space-around【项目均匀分散排列 - 两端有留白 - 留白的宽度是诸多项目之间缝隙宽度的一半】
    - space-evenly【项目均匀分散排列 - 两端有留白 - 留白的宽度与诸多项目之间缝隙宽度相等】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-b2cc4f00c564c844.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-a782d273fcba5ba9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-92594939684efcbe.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-06a380acb86e0deb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-c6ed3d0c0cb339d9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-a9108bd270ecbfbc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - align-items 定义项目在交叉轴上如何对齐

    - flex-start 【交叉轴的起点对齐】
    - flex-end 【交叉轴的重点对齐】
    - center【交叉轴中点对齐】
    - baseline 项目第一行文字的基线对齐
    - stretch【如果项目未设置高度或者高度为 auto, 则将占满整个容器高度】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-f3622b462100e103.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-b152134c26714e0c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-5e858e0c3ffff427.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-a9620dfd47e12891.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-f4474de8e61e7389.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - align-content 定义了多根轴线对其的方式， 如果项目只有一根轴线, 该属性不起作用。

    - flex-start【与交叉轴的起点对齐】
    - flex-end【与交叉轴的终点对齐】
    - center【与交叉轴中点对齐】
    - space-between【与交叉轴两端对齐, 轴线之间的间隔平均分布】
    - space-around【每根轴线两侧的间隔都相等】
    - stretch【轴线占满整个交叉轴】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-e98c420cd3da4e50.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-0956cdff47fe5dbb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-59f940b2dd143bd7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-b20387195531ca4a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-4b0835a14d3fb8c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-7bc6fc4309d8ef20.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 项目的属性 : 

  - order 定义项目的排列顺序, 数值越小越靠前

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-ea7692b4a5e2ea03.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - flex-grow 定义项目放大比例, 根据指定的比例来划分剩余的空间【默认为 0 】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-f12695624868fad0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  - flex-shrink 定义项目缩小比例, 【默认为 1 】

    ![image.png](https://upload-images.jianshu.io/upload_images/16761151-7e210125e2ccb323.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- flex-basis 定义了在分配多余空间之前, 项目占据主轴的空间。浏览器会根据这个属性来计算主轴是否有多余空间。【默认值是 auto 也即项目本来大小】

- flex 是 flex-grow 、flex-shrink、flex-basis 的简写形式。

- align-self 属性允许单个项目与其他项目有不一样的对齐方式, 可覆盖 align-items 属性。默认值为 auto。表示继承父元素的 align-items 属性, 如没有父元素则等同于 stretch，除了 auto 外其他都与 align-items 属性完全一致。

![image.png](https://upload-images.jianshu.io/upload_images/16761151-06105929889c4c6f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 23. 伪类选择器与伪类元素的区别 ? CSS3 中新增了哪些 ?





### 24. px/em/rem 的区别 ? 为什么通常给 font-size 设置的字体为 62.5%?





### 25. 如何实现图片和文字在同一行显示 ?



### 26. A 标签中 active、hover、link、visited 正确的设置顺序是什么 ?

​	

### 27. A 表标签如何禁止页面跳转 ?



### 28. 请求用 CSS 实现一个箭头/三角形 ?



### 29. 什么是精灵图 ? 如何使用 ?



### 30. meta 有哪些常见值 ? 



### 31. meta viewport 是做什么的 ? 怎么写 ?



### 32. 表单中的隐藏域【type=hidden】 有什么作用 ?



### 33. 什么场景下会出现外边距合并 ? 



### 34. 怎么去除两个按钮见的缝隙 ?



### 35. 一个页面有一排高度不一的产品图, 这时候如果我们使用 inline-block 如何使他们两端对齐 ?



### 36. 单行文本溢出 ... 如何实现 ?



### 37. 简单描述下 CSS 中的 position 定位 ?



### 38. canvas 与 svg 有什么区别 ?



### 39. CSS 预编译器是什么 ? 后编译器是什么【PostCSS】 ?



### 40. 如何让浏览器显示小于 12 px 的文字 ?



### 41. 请列举常见的 CSS 编码规范 ?



### 42. IE 盒模型和 W3C 盒模型的区别 ?



### 43. 在 input 中 name 有什么作用 ?



### 44. radio 如何实现分组 ?



### 45. CSRF 攻击是什么 ? 怎么防范 ?



### 46. 如何在页面上展示 `<div></div>` 这几个字符 ?



### 47. 前端需要注意哪些 SEO ?



### 48. `<img>` 标签的 title 与 alt 属性有什么区别 ?



### 49. 页面出现了乱码是怎么回事 ? 如何解决 ?



### 50. 简述下从浏览器地址栏输入地址键入回车到看到页面都经历了什么 ?



### 51. CSS 媒体查询是用来做什么的 ? 如何使用 ?



### 52. 请简述圣杯布局与双飞翼布局, 并手动实现 ?



### 53. BFC 是什么 ? 哪些元素或者那些属性会生成 BFC ? 



### 54. content 属性的特点 ?



### 55. 说说你了解的前端存储技术?



### 56. web worker 是什么 ? 怎么使用 ?



### 57. 有一个场景: 我们都知道商城有些数据是需要实时从后台接收来完成更新的 ?  你有几种方案来实现 ?



### 58. 请简单描述下常见的 video/audio 的 api ? 在使用过程中遇到过什么问题吗 ? 如果遇到了那么你是如何解决的 ?







## 二、API 部分



