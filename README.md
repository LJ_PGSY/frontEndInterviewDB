# 前端面试题库

> 大神也是从默默无闻历经磨难最后才声名鹊起 ~

![](https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1526037333,1469354720&fm=15&gp=0.jpg)
## 前言 :
建立此面试题库, 是为了记录市面上常见的前端面试题， 在收录与总结的过程中寻找自身技能的薄弱点并逐个击破。<br>
帮助更多的人收获自己满意的 offer，如果你是故事里的主人翁, 面对人生的抉择，手中的键盘早已沙沙作响那么欢迎你加入到我们的阵营中来。

## 简介 :
- 此面试题库预计涉及内容为 HTML/CSS、JavaScript、Vue、React, 后期可能还会扩充其他内容 ...
- 题目来源为仓库协作者们的自身经历以及社区同伴的反馈或者是国内优秀前端社区中的优质文章。
- 这样一个拥有无限魅力, 充满传奇色彩的面试题库, 需要我们共同参与完善, 欢迎各路朋友发表建议、积极参与。

## 阅前须知 :

- 本项目的 master 分支是展示线上真实版本的内容，各个模块新增的内容蕴藏在各个分支中，经过审核无误后才会合并到 master 分支上。
- 大家如果在 master 分支没有看到想看到的内容就请到各个模块的分支中寻找自己想要的内容，因为维护时间有点小紧所以可能不太及时的合并 master。但是没关系笔者会一直更下去的。
- 这是一个渐进式的前端面试仓库，里面的内容会随着时间推进不断的完善与补充，有句话说的好，不是在 coding 就是在 coding 的路上。
- 该仓库目前暂由笔者个人来维护，可能有些表述或者解析不太全面或者不正确，这也是难免的。希望大家看出之后及时的提 issue。笔者会在第一时间订正，让我们一起见证这个仓库的成长，让它帮助更多的人 !
- 如果您也是位热爱开源、对技术抱有一丝敬畏之心的话，笔者欢迎您可以加我我们一起成为仓库的贡献者或者维护者。

## 状态 :
![version](https://img.shields.io/badge/version-v1.0.0-%2348C21A)
![build](https://img.shields.io/badge/build-v1.0.0-%231081C2)
![downloads](https://img.shields.io/badge/downloads-30k%2Fweek-%23ff69b4)
![size](https://img.shields.io/badge/size-48.3%20KB-red)

## 资源目录 :

### [HTML&CSS 面试题](https://gitee.com/LJ_PGSY/frontEndInterviewDB/blob/master/src/%E6%8A%80%E6%9C%AF%E7%9B%B8%E5%85%B3/HTML&CSS/HTML&CSS.md)<br>

### [JavaScript 面试题](https://gitee.com/LJ_PGSY/frontEndInterviewDB/tree/master/src/%E6%8A%80%E6%9C%AF%E7%9B%B8%E5%85%B3/JavaScript)<br>

### [微信小程序 面试题](https://gitee.com/LJ_PGSY/frontEndInterviewDB/blob/master/src/%E6%8A%80%E6%9C%AF%E7%9B%B8%E5%85%B3/%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/%E5%89%8D%E7%AB%AF%E9%9D%A2%E8%AF%95%20-%20JavaScript%E3%80%90%E5%B0%8F%E7%A8%8B%E5%BA%8F%E7%9B%B8%E5%85%B3%E3%80%91.md)<br>

### [非技术相关面试题](https://gitee.com/LJ_PGSY/frontEndInterviewDB/blob/master/src/%E9%9D%9E%E6%8A%80%E6%9C%AF%E7%9B%B8%E5%85%B3/%E5%89%8D%E7%AB%AF%E9%9D%A2%E8%AF%95%20-%20%E9%9D%9E%E6%8A%80%E6%9C%AF%E7%9B%B8%E5%85%B3.md)<br>

loading ...

## 参与贡献 :

虚位以待 ...